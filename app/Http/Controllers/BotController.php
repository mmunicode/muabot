<?php

namespace App\Http\Controllers;

use App\Bot\Facades\Bot;
use Illuminate\Http\Request;

class BotController extends Controller
{
    public function receive(Request $request)
    {
        $data = $request->input('entry');

        //get the user’s id
        \Log::info(print_r($data, true));

        Bot::handleMessage($data[0]);
    }
}
