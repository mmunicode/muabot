<?php

namespace App\Providers;

use App\Bot\Bot;
use Illuminate\Support\ServiceProvider;

class BotServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->bind('App\Bot\Bot', function ($app) {
            return new Bot();
        });
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        return ['App\Bot\Bot'];
    }
}