<?php

namespace App\Bot;

use pimax\FbBotApp;

class Bot
{
    protected $msg = ["text" => ''];

    public function __construct()
    {
    }
/*     [entry] => Array
    (
        [0] => Array
            (
                [id] => 298551551084975
                [time] => 1568520343736
                [messaging] => Array
                    (
                        [0] => Array
                            (
                                [sender] => Array
                                    (
                                        [id] => 2747936538563484
                                    )

                                [recipient] => Array
                                    (
                                        [id] => 298551551084975
                                    )

                                [timestamp] => 1568520343379
                                [message] => Array
                                    (
                                        [mid] => 6XJsF4JhJKq738SUkARpR4dz7qrNHqaqlPJjuKRFWO5Xgcr_3FUCaJkusdo-A_V9ypi82_ZcSDR8IIEgj8EraA
                                        [text] => Hi
                                    )

                            )

                    )

            )

    )
 */
    public function handleMessage(array $data)
    {
        $response = [];

        if (!empty($data)) {
            $messaging = $data["messaging"][0];

            if (isset($messaging["postback"])) {
                if ($messaging["postback"]["payload"] == "get-started") {
                    $request = $this->populateQuestion();
                    $response = $request;

                    return $this->sendMessage($messaging["sender"]["id"], $response);
                } else {
                    $senderId = $messaging["sender"]["id"];
                    $payload = $messaging["postback"]["payload"];
                    return $this->handlePayLoad($senderId, $payload);
                }
            }

            /* if (isset($messaging["message"])) {
                $response = ["text" => "မင်္ဂလာပါ မြန်မာယူနီကုဒ် ဧရိယာ အကူအညီမှ ကြိုဆိုပါသည်။"];
                return $this->sendMessage($messaging["sender"]["id"], $response);
            } */
        }
    }

    private function handlePayLoad($senderId, $payload)
    {
        $response = [];

        if ($payload == "phone") {
            $response = ["text" => "ကျေးဇူးပြု၍ လူကြီးမင်း၏ ဖုန်း setting ထဲမှ about ကို ပုံရိုက်၍ ပို့ပေးပါ။ အကူအညီပေးနိုင်ရန် ပြန်လည်ဆက်သွယ်ပေးပါ့မည်။"];
            return $this->sendMessage($senderId, $response);
        }

        if ($payload == "computer") {
            //
        }

        if ($payload == "font") {
            $response = ["text" => "ဒီမှ https://www.mmunicode.org/wiki/myanmar-unicode-fonts/  ရယူပါ။"];
            return $this->sendMessage($senderId, $response);
        }
    }

    private function sendMessage($recipientId, $data)
    {
        $messageData = [
            "recipient" => [
                "id" => $recipientId
            ],
            "message" => is_string($data) ? ["text" => $data] : $data
        ];

        \Log::info(print_r($messageData, true));

        $ch = curl_init('https://graph.facebook.com/v2.6/me/messages?access_token=' . env("PAGE_ACCESS_TOKEN"));
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_HEADER, false);
        curl_setopt($ch, CURLOPT_HTTPHEADER, ["Content-Type: application/json"]);
        curl_setopt($ch, CURLOPT_POST, true);
        curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($messageData));
        curl_exec($ch);
    }

    private function populateQuestion()
    {
        //compose message
        $text = htmlspecialchars_decode("ကျေးဇူးပြု၍ ရွေးချယ်ပေးပါ။  က်ေးဇူးျပဳ၍ ေရြးခ်ယ္ေပးပါ။", ENT_QUOTES | ENT_HTML5);

        $response = [
            "attachment" => [
                "type" => "template",
                "payload" => [
                    "template_type" => "button",
                    "text" => $text,
                    "buttons" => []
                ]
            ]
        ];

        $letters = [
            "computer" => "computer",
            "phone" => "phone",
            "font" => "ဖောင့်များရယူရန်"
            //"get-started" => "get-started"
        ];
        foreach ($letters as $key => $option) {
            $response["attachment"]["payload"]["buttons"][] = [
                "type" => "postback",
                "title" => $option,
                "payload" => $key
            ];
        }

        return $response;
    }
}
