<?php

namespace App\Bot\Facades;

use Illuminate\Support\Facades\Facade;

class Bot extends Facade
{
    protected static function getFacadeAccessor()
    {
        return 'App\Bot\Bot';
    }
}
